﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetBox : MonoBehaviour
{
	// Start is called before the first frame update
	//The box's current health point total
	public int currentHealth = 1;
	public int score;
	static Text scoreText;

    private void Start()
    {
		scoreText = GameObject.Find("ScoreText").GetComponentInChildren<Text>();
	}

    public void Damage(int damageAmount)
	{
		//subtract damage amount when Damage function is called
		currentHealth -= damageAmount;

		//Check if health has fallen below zero
		if (currentHealth <= 0)
		{
			//if health has fallen below zero, deactivate it 
			gameObject.SetActive(false);
		}
	}
}
