﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengeModTimer : MonoBehaviour
{
    public static Text timerText;
    private Coroutine co;

    // Start is called before the first frame update
    void Start()
    {
        timerText = GameObject.FindGameObjectWithTag("TimerText").GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator timingSeconds(float timeValue)
    {
        while (timeValue > 0)
        {
            timeValue -= 1;
            timerText.text = "Challenge Mod : " + timeValue+ "sec";
            yield return new WaitForSeconds(1f);
        }
        timerText.text = "";
    }


    public void launchTimer(float timeValue)
    {
        co = StartCoroutine(timingSeconds(timeValue));
    }

    public void stopTimer()
    {
        StopCoroutine(co);
        timerText.text = "";
    }
}
