﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingTargetsSpawner : MonoBehaviour
{
    public float spawnspeed = 1f;
    public GameObject target;
    public GameObject wall;
    private float marge = 80f;
    Coroutine currentMod;
    private float spawnInterval;
    public static Text accuracyText;
    public float totalFire;
    public float hitFire;
    public float accuracy;

    // Start is called before the first frame update
    void Start()
    {
        accuracyText = GameObject.Find("Accuracy").GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        // Vitesse comprise entre 1 et 100
        spawnspeed = Mathf.Clamp(spawnspeed, 10f, 90f);

        // Change l'intervalle de spawn en fonction de la vitesse (délai max à 5 secondes)
        switch (spawnspeed)
        {
            case 0f:
                spawnInterval = 5f;
                break;
            case 10f:
                spawnInterval = 4f;
                break;
            case 20f:
                spawnInterval = 3f;
                break;
            case 30f:
                spawnInterval = 2f;
                break;
            case 40f:
                spawnInterval = 1.5f;
                break;
            case 50f:
                spawnInterval = 1.2f;
                break;
            case 60f:
                spawnInterval = 1f;
                break;
            case 70f:
                spawnInterval = 0.7f;
                break;
            case 80f:
                spawnInterval = 0.5f;
                break;
            case 90f:
                spawnInterval = 0.4f;
                break;
            case 100f:
                spawnInterval = 0.3f;
                break;
        }
    }

    // Déplacement du spawner de cible et instantiation d'une cible bien orientée
    IEnumerator setTargets(string mod)
    { 
        float wallX = wall.transform.position.x;
        float wallY = wall.transform.position.y;
        float minX = -(target.transform.localScale.x / 2) - wall.transform.localScale.x / 2 + wallX;
        float maxX = (wall.transform.localScale.x / 2) + target.transform.localScale.x / 2 + wallX;
        float minY = -(target.transform.localScale.y / 2) - wall.transform.localScale.y / 2 + wallY;
        float maxY = (wall.transform.localScale.y / 2) + target.transform.localScale.y / 2 + wallY;
        bool keepSpawning = true;
        int targetsNumber = 0;

        while(keepSpawning)
        {
            Vector3 nextPos = new Vector3(Random.Range(minX - marge, maxX + marge), Random.Range(minY + marge, maxY - marge), wall.transform.position.z);
            transform.position = nextPos;
            Vector3 targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - wall.transform.localScale.z);
            Instantiate(target, targetPosition, transform.rotation * Quaternion.Euler(90f, 0f, 0f));
            targetsNumber++;

            // Le challenge mode s'arrete au bout de 30 secondes
            if(spawnInterval * targetsNumber > 30 && mod == "challenge")
            {
                keepSpawning = false;
                accuracy = (hitFire / totalFire) * 100;
                accuracyText.text = "Accuracy: "+accuracy+"%";
            }
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    public void launchTrainMod()
    {
        currentMod = StartCoroutine(setTargets("train"));
    }

    public void launchChallengeMod()
    {
        currentMod = StartCoroutine(setTargets("challenge"));
    }

    public void stopCurrentMod()
    {

        StopCoroutine(currentMod);
    }

    public void setTotalFire(float total)
    {
        totalFire = total;
    }

    public void setHitFire(float hit)
    {
        hitFire = hit;
    }
}
