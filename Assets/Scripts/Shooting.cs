﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;


public class Shooting : MonoBehaviour
{
    // Set the number of hitpoints that this gun will take away from shot objects with a health script
    public int gunDamage = 1;

    // Number in seconds which controls how often the player can fire
    public float fireRate = 0.25f;

    // Distance in Unity units over which the player can fire
    public float weaponRange = 50f;

    // Amount of force which will be added to objects with a rigidbody shot by the player
    public float hitForce = 100f;

    // Holds a reference to the gun end object, marking the muzzle location of the gun
    public Transform gunEnd;

    // Holds a reference to the first person camera
    public Camera fpsCam;

    // WaitForSeconds object used by our ShotEffect coroutine, determines time laser line will remain visible
    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);

    // Line render of the shoot
    private LineRenderer laserLine;                                     
    private float nextFire;

    public MovingCamera cameraScript;
    public MovingTargetsSpawner spawnScript;
    public ChallengeModTimer chronoScript;

    public bool trainMod;
    public bool challengeMod;

    public static Text sensitivityText;
    public static Text speedText;
	public static Text scoreText;

    public float timeRemaining = 30;
    public bool timerIsRunning = false;
    public float totalFire;
    public float hitFire;
    public float accuracy;
    public int score = 0;

    public AudioSource audio;


    // Start is called before the first frame update
    void Start()
    {
        // Get and store a reference to our LineRenderer component
        laserLine = GetComponent<LineRenderer>();
        sensitivityText = GameObject.Find("SensitivityText").GetComponentInChildren<Text>();
        speedText = GameObject.Find("SpeedText").GetComponentInChildren<Text>();
        scoreText = GameObject.Find("ScoreText").GetComponentInChildren<Text>();
        sensitivityText.text = "Sensitivity : " + cameraScript.mouseSens.ToString();
        speedText.text = "Speed : " + spawnScript.spawnspeed.ToString() + "%";
        trainMod = false;
        challengeMod = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Check if the player has pressed the fire button and if enough time has elapsed since they last fired
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            audio.Play();
            totalFire++;
            
            // Update the time when our player can fire next
            nextFire = Time.time + fireRate;

            // Start our ShotEffect coroutine to turn our laser line on and off
            StartCoroutine(ShotEffect());

            // Create a vector at the center of our camera's viewport
            Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));

            // Declare a raycast hit to store information about what our raycast has hit
            RaycastHit hit;

            // Set the start position for our visual effect for our laser to the position of gunEnd
            laserLine.SetPosition(0, gunEnd.position);

            // Check if our raycast has hit anything
            if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, weaponRange))
            {
                // Set the end position for our laser line 
                laserLine.SetPosition(1, hit.point);

                switch (hit.collider.tag)
                { 
                    case "Target": 
                        // Get a reference to a health script attached to the collider we hit
                        TargetBox health = hit.collider.GetComponent<TargetBox>();

                        // If there was a health script attached
                        if (health != null)
                        {
                            // Call the damage function of that script, passing in our gunDamage variable
                            health.Damage(gunDamage);
                            hitFire++;
                        }

                        // Check if the object we hit has a rigidbody attached
                        if (hit.rigidbody != null)
                        {
                            // Add force to the rigidbody we hit, in the direction from which it was hit
                            hit.rigidbody.AddForce(-hit.normal * hitForce);
                        }

                        score++;
			            scoreText.text = "SCORE: " + score.ToString();

                        break;

                    case "SensiUp":
                        cameraScript.mouseSens = Mathf.Clamp(cameraScript.mouseSens, 30f, 500f);
                        cameraScript.mouseSens += 20f;
                        sensitivityText.text = "Sensitivity : " + cameraScript.mouseSens.ToString();
                        break;

                    case "SensiDown":
                        cameraScript.mouseSens = Mathf.Clamp(cameraScript.mouseSens, 30f, 500f);
                        cameraScript.mouseSens -= 20f;
                        sensitivityText.text = "Sensitivity : " + cameraScript.mouseSens.ToString();
                        break;

                    case "SpeedUp":
                        spawnScript.spawnspeed += 10f;
                        speedText.text = "Speed : " + spawnScript.spawnspeed.ToString() + "%"; 
                        break;

                    case "SpeedDown":
                        spawnScript.spawnspeed -= 10f;
                        speedText.text = "Speed : " + spawnScript.spawnspeed.ToString() + "%";
                        break;

                    case "Reset":
                        score = 0;
                        scoreText.text = "SCORE: 0";
                    break;

                    case "TrainMod":
                        trainMod = !trainMod;
                        if (trainMod)
                        {
                            if (challengeMod)
                            {
                                challengeMod = false;
                                chronoScript.stopTimer();
                                spawnScript.stopCurrentMod();
                            }
                            spawnScript.launchTrainMod();
                        }
                        else
                        {
                            spawnScript.stopCurrentMod();
                        }
                        break;

                    case "ChallengeMod":
                        challengeMod = !challengeMod;
                        totalFire = 0;
                        hitFire = 0;
                        if (challengeMod)
                        {
                            chronoScript.launchTimer(timeRemaining);
                            if(trainMod)
                            {
                                trainMod = false;
                                spawnScript.stopCurrentMod();
                            }
                            spawnScript.launchChallengeMod();
                        }
                        else
                        {
                            chronoScript.stopTimer();
                            spawnScript.stopCurrentMod();

                        }
                        break;

                }
                spawnScript.setHitFire(hitFire);
                spawnScript.setTotalFire(totalFire);
            }
            else
            {
                // If we did not hit anything, set the end of the line to a position directly in front of the camera at the distance of weaponRange
                laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * weaponRange));
            }
        }
        
    }

    private IEnumerator ShotEffect()
    {
        // Turn on our line renderer
        laserLine.enabled = true;

        //Wait for .07 seconds
        yield return shotDuration;

        // Deactivate our line renderer after waiting
        laserLine.enabled = false;
    }
}
