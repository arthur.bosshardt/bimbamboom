﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestruction : MonoBehaviour
{
    public float delayDestruction;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, delayDestruction);
    }

    // Update is called once per frame
    void Update()
    {

    }
}