﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movementX = Camera.main.transform.right * moveHorizontal;
        Vector3 movementZ = Camera.main.transform.forward * moveVertical;
        Vector3 movement = movementX + movementZ;
        rb.AddForce(movement * speed, ForceMode.VelocityChange);
        rb.velocity = Vector3.zero;
    }
}
