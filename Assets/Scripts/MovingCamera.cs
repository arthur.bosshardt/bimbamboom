﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCamera : MonoBehaviour
{
    public float mouseSens = 80f;
    public Camera playerCamera;
    float xRotation = 0f;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

        transform.Rotate(Vector3.up * mouseX);

        xRotation = Mathf.Clamp(xRotation, -90f, 80f);
        xRotation -= mouseY;
        playerCamera.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);        
    }
}