# Bimbamboom

Aimlab for professional players.

## Description of aimlab

**Aim Lab** is the ultimate FPS training solution. Built by neuroscientists, Aim Lab blends cutting-edge performance tracking and analytics with AI-based training to make you better, faster, stronger.

## Rules

The objective of **Bimbamboom** is to get the higher score. Destroy a target give you points so you have to be accurate !

GL & HF 🤟


